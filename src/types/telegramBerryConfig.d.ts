interface TelegramBerryConfig {
  telegramToken: string;
  telegramId: string | number;
}
