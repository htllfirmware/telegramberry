import Telegram from './telegram';
import TelegramBot from 'node-telegram-bot-api';

async function ping(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  await telegram.bot.sendMessage(chatId, 'yo');
}

async function echo(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    telegram.dialog = {
      type: 'none'
    };
    await telegram.bot.sendMessage(chatId, msg.text);
  } catch (e) {
    await telegram.bot.sendMessage(chatId, 'offline');
  }
}

async function initEcho(telegram: Telegram, msg: TelegramBot.Message) {
  const chatId = msg.chat.id;
  if (chatId !== telegram.telegramId) {
    return;
  }
  try {
    telegram.dialog = {
      type: 'theDialog'
    };
    await telegram.bot.sendMessage(chatId, 'send some text');
  } catch (e) {
    await telegram.bot.sendMessage(chatId, e.message);
  }
}


async function messageProcessing(telegram: Telegram, msg: TelegramBot.Message) {
    if (telegram.dialog.type === 'theDialog') {
      await echo(telegram, msg);
    }
}

export default function telegramCommandProcessing(telegram: Telegram): void {
  telegram.bot.onText(/^\/ping$/, (msg: TelegramBot.Message) => ping(telegram, msg));
  telegram.bot.onText(/^\/echo$/, (msg: TelegramBot.Message) => initEcho(telegram, msg));
  telegram.bot.on('message', (msg: TelegramBot.Message) => messageProcessing(telegram, msg));
}
