import config from 'config';
import Telegram  from './lib/telegram';
import telegramCommandProcessing  from './lib/telegramCommandProcessing';
const telegramBerryConfig = config.get<TelegramBerryConfig>('telegramBerry');
const telegram = new Telegram(telegramBerryConfig.telegramToken, telegramBerryConfig.telegramId,
    (_telegram: Telegram) => telegramCommandProcessing(_telegram));
const main = (async () => {
  await telegram.sendMessage('telegramBerry is online');
  console.log('telegramBerry is online');
});
main();
