#!/usr/bin/env bash
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - 2>&1
sudo apt-get install -y nodejs
sudo apt-get install build-essential
cd /usr/src/payload
npm install
npm build
